// Contruct an array of { name, population and area }. And
// sort them out in terms of area .
// If area is same . Sort by population.[{name: 'detroit', populations:"18M", area :''}, {name : ""}]
// name: Los Angeles 
const cities = {
    detroit: {
    populations: "18M",
    area: "72M Sq Acres" 
    },
    los_angeles: {
    populations: "11M",
    area: "72M Sq Acres"
    },
    ontario: {
    populations: "28M",
    area: "108M Sq Acres"
    },
    san_diego: {
    populations: "23M",
    area: "67M Sq Acres"
    },
    san_francisco: {
    populations: "18M",
    area: "87M Sq Acres"
    },
}
let sortFnc=require('./sort');

let result=sortFnc(cities);
console.log(result);


   