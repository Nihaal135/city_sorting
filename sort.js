// Contruct an array of { name, population and area }. And
// sort them out in terms of area .
// If area is same . Sort by population.
// name: Los Angeles 

function separateFnc(areaValue){
    let output=[];
    const splitValue=areaValue.split(' ');
    for(let index=0;index<splitValue.length;index++){
       if('0' <= splitValue[index] <= '9'|| splitValue[index]==='.'){
           output.push(splitValue[index]);
       }
    }
    return parseFloat(output.join());
}

function mainSortFunction(inventory){
   if(!inventory){
       return {};
   }
   let newArray=[];
   for(let keys in inventory){
       let tempObject = {"name" : keys, ...inventory[keys]};
       newArray.push(tempObject);
   }
   newArray.sort(function(firstValue,secondValue){
       if(separateFnc(firstValue.area) === separateFnc(secondValue.area)){
           if(separateFnc(firstValue.populations)>separateFnc(secondValue.populations)){
               return 1;
           }
           else{
               return -1;
           }
       }
       else if(separateFnc(firstValue.area) >separateFnc(secondValue.area)){
           return 1;
       }
       else{
           return -1;
       }
   })
   //return  newArray;
   return newArray.reduce(function(first,second){
    first[second['name']]={"populations":second.populations,"area":second.area};
    return first;
   },{})
}

module.exports=mainSortFunction;
